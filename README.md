# Exercices de manip de DOM

### Auteur
Diane (Mogw4i)

## Langages
    - JavaScript
    - HTML
    - (CSS)

# Exo1
Partie JS : génère entièrement un tableau selon une entrée utilisateur; chaque cellule contient un lien indexé selon sa
position dans le tableau.

# Exo 2
Sans feuille de style, la partie JS permet, une fois le bouton enclenchéé, de changer la couleur et le fond du paragraphe.

# Exo 3
Script qui affiche dans la console les valeurs du formulaire (sans ID).

# Exo 4
## V0.1 
Site qui ajoute une rangée à un tableau à chaque enclenchement du bouton.
## V0.2
L'utilisateur peut désormais choisir une cellule (via sa colonne et rangée) et écrire le texte qu'il souhaite à l'intérieur.

# Exo 5
Sur clic d'un bouton, supprime le champ sélectionné d'un menu déroulant.                        

